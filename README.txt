PROJET LILA CAPPELIE ADELIE FERRE

Ce projet réalisé sur le thème de infinite loops représente des mots interagissants entre eux en suivant un comportement algorithmique de nuée proche de celui des oiseaux (boids).

Notre projet se découpe en plusieurs phases d'interactions liées au clic.
La première phase est constituée de mots bleus sur fond gris foncé. Le clic provoque l'apparition de nouveau boids rouges grâce à la fonction mouseDragged qui ajoute des Boids au tableau de boids. 
Une fois le nombre de boids ajoutés par l'utilisateur égal à 40, un rectangle bornant les boids apparait et les bloque (wraparound). 
Des ellipses en faible opacité et dont la vitesse est gérée avec du Noise apparaissent dans le fond.

L'utilisateur peut continuer à cliquer pour générer d'autres boids qui s'agglutinnent dans cette case. 
Une autre condition liée au nombre de boids gère l'arrivée à la prochaine phase : lorsque le nombre de boids est égal à 120, la case s'agrandit et les boids reprennent leur liberté initiale, les ellipses disparaissent.


Sans action de la part de l'utilisateur, le code boucle indéfiniment sur la phase où les boids ont été laissés.

Typographie utilisée : Director - EESAB