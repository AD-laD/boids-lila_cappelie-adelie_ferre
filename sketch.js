// mots liés à l'action négative de l'Homme sur l'environnement
var textdata1=["AGW","gas","USHCN","INDC","energ","temperature","carbon","pollution","co2","politics","emission","weather","fossil","fuel","sea-level rise","methane","warm","degre","cool","dioxid","barrel","oil","melt","megawatt","reactor","nuclear","cyclon","storm","hurrican","scheme","cultivar","endanger","extinct","vehicl","electric","car","millenni","mercuri","flood","cloud","ratif","treati","impact","EPA","acid","simul","CLF","NHTSA","MGP","diseas","VMT"];

// mots liés à la nature
var textdata2=["climate","health","antarct","glacier","antarctica","coral", "phytoplankton","bear","polar","forest","species", "environment","atmosphe","Earth","ozon"]

// mots liés à l'action positive de l'Homme sur l'environnement
var textdata3=["conservation","consensus","alarmist","develop","recycle","hydrogen","turbin","wind","COP","UNFCCC","IPCC","PPM","science","integrity","mitigation","mediev","renew","green", "palaeo","adapt","EIA","GHG","calcif","RGGI","NAAQ","NDVI","USHCN"]


let flock;
let neighbordist;

let boidtxt, boidtxt2;
let mouseindex =0;
let energy, environment;
let identity;

var idtab=["energy","environment"];

function preload() {
  fontBold = loadFont('assets/Director-bold.ttf');
  fontLight = loadFont('assets/Director-Light.ttf');
  fontRegular = loadFont('assets/Director-Regular.ttf');
}

let fonttab=["fontRegular", "fontBold", "fontLight"];

let indice=0; // permet de controler les boids ajoutés
let rouge=0; // nombre ajouter à la composante rouge des points du fond
let noiseValX=0;
let noiseValY=0;
let bornes=[0,0,750,576]; // bornes du canva

// setInterval(window.location.reload(), 30000);

function setup() {
  var cnv=createCanvas(750, 576);
  cnv.style('display', 'block');
  background(30);
  
  setTimeout(start, 3000);

  flock = new Flock();
  // Add an initial set of boids into the system
   for (let j = 0; j < textdata2.length; j++) {
    boidtxt2=textdata2[j];
    //console.log(boidtxt);
    let c = new Boid(width / 4,height / 4, boidtxt2, 1);
    flock.addBoid(c);
  }
  
}

function draw() {
    // dessin du fond
  if(indice>200){
    rouge+=0.5;
    if(bornes[0]<200){
      bornes[0]+=1;
      bornes[1]+=1;
    }
    
  }
  if(indice>600){
    rouge+=-0.8;
    if(bornes[0]>0){
      bornes[0]-=1.4;
      bornes[1]-=1.4;
    }
  }
  if(rouge>=0){
    fill(0,100,100);
    for(let i=0;i<1500;i+=80){
      for(let j=0;j<1162;j+=80){  
        noiseValX=random(0,2);
        noiseValY=random(0,2);
        //noiseValY-=0.001;
        noStroke();
        fill(30+rouge,30,30,50);
        circle(i+noise(noiseValX)*100,j+noise(noiseValY)*100,50);
        //circle(i,j,50);
    }
  }
  }
  else{noiseValx=0;noiseValY=0}
  //print(indice);


  //textSize(random(10,30));
    //blendMode(DIFFERENCE);
  fill(30, 50);
  rect(0, 0, width, height);
  //blendMode(DIFFERENCE);
  fill(200,50);
  rect(0,0,bornes[0],576);
  rect(bornes[0],0,750,bornes[1]);
  if(bornes[0]<=0 && indice>600){
    fill(30);
    rect(0,0,750,576);
  }
  

  flock.run();

}

function start(boids){
  setTimeout(start, 3000);
  neighbordist=100;
}

// Add a new boid into the System
function mouseDragged() {
  if(mouseX>bornes[0] || mouseY>bornes[1]){
    // on vérifie qu'on se trouve bien entre les bornes
    if(indice%5==0){
      // on ajoute un boids une fois sur 5 pour ne pas surcharger
      if(mouseindex<11){ //faut mettre la taille du tableau-1
        mouseindex+=1;
      }else{
        mouseindex+=1;
        mouseindex=mouseindex%(12);
      }
      
      let d;
      if(indice<600) d = new Boid(mouseX-90,mouseY-90,textdata1[mouseindex%(textdata1.length)],0)
      else d =new Boid(mouseX-90, mouseY-90, textdata3[mouseindex%(textdata3.length)], 1)
      flock.addBoid(d);
    }
    indice++;
  }
}

// The Nature of Code
// Daniel Shiffman
// http://natureofcode.com

// Flock object
// Does very little, simply manages the array of all the boids

function Flock() {
  // An array for all the boids
  this.boids = []; // Initialize the array
}

Flock.prototype.run = function() {
  for (let i = 0; i < this.boids.length; i++) {
    this.boids[i].run(this.boids);  // Passing the entire list of boids to each boid individually
  }
}

Flock.prototype.addBoid = function(b) {
  this.boids.push(b);
}

// The Nature of Code
// Daniel Shiffman
// http://natureofcode.com

// Boid class
// Methods for Separation, Cohesion, Alignment added

function Boid(x, y, boidtxt, identity) {
  this.acceleration = createVector(0, 0);
  this.velocity = createVector(random(-1, 1), random(-1, 1));
  this.position = createVector(x, y);
  this.maxspeed = 1.5;    // Maximum speed
  this.maxforce = 0.05; // Maximum steering force
  this.boidtxt=str(boidtxt);
  this.size=random(20,30);
  this.fontstyle=random(fonttab);
  this.identity=identity;
  this.r = 1;
  this.posreel = createVector(x+this.rh, y+this.rl);
}

Boid.prototype.run = function(boids) {
  this.flock(boids);
  this.update();
  this.borders();
  this.render(); //appelée dans le draw
}

Boid.prototype.applyForce = function(force) {
  // We could add mass here if we want A = F / M
  this.acceleration.add(force);
}

// We accumulate a new acceleration each time based on three rules
Boid.prototype.flock = function(boids) {
  let sep = this.separate(boids);   // Separation
  let ali = this.align(boids);      // Alignment
  let coh = this.cohesion(boids);   // Cohesion
  // Arbitrarily weight these forces
  sep.mult(1.5);
  ali.mult(1.0);
  coh.mult(1.0);
  // Add the force vectors to acceleration
  this.applyForce(sep);
  this.applyForce(ali);
  this.applyForce(coh);
}

// Method to update location
Boid.prototype.update = function() {
  // Update velocity
  this.velocity.add(this.acceleration);
  // Limit speed
  this.velocity.limit(this.maxspeed);
  this.position.add(this.velocity);
  // Reset accelertion to 0 each cycle
  this.acceleration.mult(0);
}

// A method that calculates and applies a steering force towards a target
// STEER = DESIRED MINUS VELOCITY
Boid.prototype.seek = function(target) {
  let desired = p5.Vector.sub(target,this.position);  // A vector pointing from the location to the target
  // Normalize desired and scale to maximum speed
  desired.normalize();
  desired.mult(this.maxspeed);
  // Steering = Desired minus Velocity
  let steer = p5.Vector.sub(desired,this.velocity);
  steer.limit(this.maxforce);  // Limit to maximum steering force
  return steer;
}

Boid.prototype.render = function() {
  // Draw a triangle rotated in the direction of velocity
  let theta = this.velocity.heading() + radians(90);
  let test = this.identity;
  if(test==0){
      fill(255,0,0);
  } if(test==1) {
      fill(37,40,187);
  }
  if(test==2){
    fill(0,0);
  }
  //stroke(200);
  push();
  translate(this.position.x, this.position.y);
  //rotate(theta);
   textSize(this.size);
  textFont(this.fontstyle);
  text(this.boidtxt, this.position.x, this.position.y);
 // beginShape();
 //vertex(0, -this.r * 2);
 // vertex(-this.r, this.r * 2);
 // vertex(this.r, this.r * 2);
 //endShape(CLOSE);
  pop();

}

// Wraparound
Boid.prototype.borders = function() {
  // Conditons de wraparound
  if(indice<200){
  if (this.position.x < -40)  this.position.x = width-350;
  if (this.position.y < -40)  this.position.y = height-240;
  if (this.position.x > width-300) this.position.x = -20;
  if (this.position.y > height-100) this.position.y = 0;
  }
  if(indice<600) {
    if (this.position.x < bornes[0]-40)  this.position.x = bornes[2]-350;
    else if (this.position.y < bornes[1]-40)  this.position.y = bornes[3]-240;
    else if (this.position.x > width-300){
      this.position.x = 10; 
      this.position.y = this.position.y%100;
      this.velocity=createVector(0,0);
    }
    else if (this.position.y > height-100){
      this.position.y = 40; 
      this.position.x = random(width);
      this.velocity=createVector(0,0);
    }
  }
  else{
    if(bornes[0]<=0){
      this.identity=2;
      window.location.reload();
    }
    
  }
}


// Separation
// Method checks for nearby boids and steers away
Boid.prototype.separate = function(boids) {
  let desiredseparation = 25.0;
  let steer = createVector(0, 0);
  let count = 0;
  // For every boid in the system, check if it's too close
  for (let i = 0; i < boids.length; i++) {
    let d = p5.Vector.dist(this.position,boids[i].position);
    // If the distance is greater than 0 and less than an arbitrary amount (0 when you are yourself)
    if ((d > 0) && (d < desiredseparation)) {
      // Calculate vector pointing away from neighbor
      let diff = p5.Vector.sub(this.position, boids[i].position);
      diff.normalize();
      diff.div(d);        // Weight by distance
      steer.add(diff);
      count++;            // Keep track of how many
    }
  }
  // Average -- divide by how many
  if (count > 0) {
    steer.div(count);
  }

  // As long as the vector is greater than 0
  if (steer.mag() > 0) {
    // Implement Reynolds: Steering = Desired - Velocity
    steer.normalize();
    steer.mult(this.maxspeed);
    steer.sub(this.velocity);
    steer.limit(this.maxforce);
  }
  return steer;
}

// Alignment
// For every nearby boid in the system, calculate the average velocity
Boid.prototype.align = function(boids) {
  //neighbordist = boids.size;
  neighbordist = 100;
  let sum = createVector(0,0);
  let count = 0;
  for (let i = 0; i < boids.length; i++) {
    let d = p5.Vector.dist(this.position,boids[i].position);
    if ((d > 0) && (d < neighbordist)) {
      sum.add(boids[i].velocity);
      count++;
    }
  }
  if (count > 0) {
    sum.div(count);
    sum.normalize();
    sum.mult(this.maxspeed);
    let steer = p5.Vector.sub(sum, this.velocity);
    steer.limit(this.maxforce);
    return steer;
  } else {
    return createVector(0, 0);
  }
}

// Cohesion
// For the average location (i.e. center) of all nearby boids, calculate steering vector towards that location
Boid.prototype.cohesion = function(boids) {
  let neighbordist = 50;
  let sum = createVector(0, 0);   // Start with empty vector to accumulate all locations
  let count = 0;
  for (let i = 0; i < boids.length; i++) {
    let d = p5.Vector.dist(this.position,boids[i].position);
    if ((d > 0) && (d < neighbordist)) {
      sum.add(boids[i].position); // Add location
      count++;
    }
  }
  if (count > 0) {
    sum.div(count);
    return this.seek(sum);  // Steer towards the location
  } else {
    return createVector(0, 0);
  }
}


